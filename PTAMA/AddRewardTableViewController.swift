//
//  AddRewardTableViewController.swift
//  PTAMA
//
//  Created by Joshua Truscott on 4/6/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase

class AddRewardViewController: UITableViewController {
    

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var cost: UITextField!
    @IBOutlet weak var numAvailable: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let saveTaskButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveReward))
        self.navigationItem.rightBarButtonItem = saveTaskButton
        self.name.addDoneButtonOnKeyboard()
        // Do any additional setup after loading the view.
    }
    
    @objc func saveReward() {
        if let patient = UserDefaults.standard.string(forKey: "currentPatient") {
            let rewardValues = ["description": name.text as Any, "cost": Int(cost.text!) as Any, "numAvailable": Int(numAvailable.text!) ?? -1 as Any]
            let ref = Database.database().reference().child("rewards").child(patient).childByAutoId()
            ref.updateChildValues(rewardValues
                , withCompletionBlock: { (error, ref) in
                    if error != nil {
                        print(error!)
                        return
                    }
                    if let navController = self.navigationController {
                        navController.popViewController(animated: true)
                    }
            })
            
        }
        
        
        
    }
    
}
