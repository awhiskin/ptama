//
//  ProfileViewController.swift
//  PTAMA
//
//  Created by Joshua Truscott on 8/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var profileNameLabel: UILabel!
    
    var profileLocked: Bool = true
    
    /**
     Get name/email to display on the profileNameLabel
     
     - TODO: Maybe change this to show patient name?
     */
    func getProfileDetails() {
        let caregiver = Auth.auth().currentUser
        
        profileNameLabel.text = "Welcome, " + (caregiver?.email)!
    }
    
    /**
     Get patient details
     */
    func getPatientName() {
        let currentPatient = UserDefaults.standard.string(forKey: "currentPatient")
        let currentCaregiver = UserDefaults.standard.string(forKey: "currentCaregiver")
        let databaseRef = Database.database().reference().child("caregivers").child(currentCaregiver!).child("patients").child(currentPatient!)
        databaseRef.observeSingleEvent(of: .value, with: { snapshot in
            if let data = snapshot.value as? [String : AnyObject] {
                let name = data["name"] as! String
                self.profileNameLabel.text = "Welcome, " + name
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileLocked = true
        lockProfile()
        
        getPatientName()
    }
    
    /* ------ PROFILE LOCK / UNLOCK MANAGEMENT ---------- */
    
    @IBAction func lockIconTouched(_ sender: UIBarButtonItem) {
        toggleLocked()
    }
    
    /**
     Toggles the locked status of the profile.
     
     - TODO: finalise alert message text
     */
    func toggleLocked() {
        if profileLocked {
            // prompt with alert
            let alert = UIAlertController(title: "Unlock profile?", message: "To unlock your profile, press OK to continue.", preferredStyle: UIAlertControllerStyle.alert)
            
            // if they proceed with unlock, call unlockProfile handler
            alert.addAction(UIAlertAction(title: "OK",
                                          style: UIAlertActionStyle.default,
                                          handler: unlockProfile))
            
            // if they cancel unlock, do nothing
            alert.addAction(UIAlertAction(title: "Cancel",
                                          style: UIAlertActionStyle.cancel,
                                          handler: nil))
            
            // present alert to user
            self.present(alert, animated: true, completion: nil)
        } else {
            lockProfile()
        }
    }
    
    @IBOutlet weak var navBarLockIcon: UIBarButtonItem!
    @IBOutlet weak var profileInfoContainerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var logOutButton: UIBarButtonItem!
    
    var lockedIcon = #imageLiteral(resourceName: "icon-lock-locked")
    var unlockedIcon = #imageLiteral(resourceName: "icon-lock-unlocked")
    var animateDuration = 0.25
    
    /**
     Locks user profile to prevent actions such as logging out and editing tasks/rewards.
     Profile is locked by default.
     
     - TODO: set a UserDefault value if necessary
     */
    func lockProfile() {
        // if already unlocked, lock the profile
        navBarLockIcon.image = lockedIcon
        profileLocked = true
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: self.animateDuration, animations: {
                self.profileInfoContainerView.alpha = 0
            }, completion: {
                (value:Bool) in
                self.profileInfoContainerView.isHidden = true
            })
            self.logOutButton.isEnabled = false
        }
    }
    
    /**
     Unlocks user profile to allow actions such as logging out and editing tasks/rewards
     
     - TODO: set a UserDefault value if necessary
     */
    func unlockProfile(_ action: UIAlertAction) {

        navBarLockIcon.image = unlockedIcon
        profileLocked = false
        DispatchQueue.main.async {
            self.profileInfoContainerView.isHidden = false
            UIView.animate(withDuration: self.animateDuration) {
                self.profileInfoContainerView.alpha = 1
            }
            self.logOutButton.isEnabled = true
        }
    }
    
    
    /* ------ LOGOUT MANAGEMENT ---------- */
    
    @IBAction func logOutPressed(_ sender: AnyObject) {
        
        let alert = UIAlertController(title: "Logout?", message: "To log out of your profile, press OK to continue.",
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        // if they proceed with unlock, call logoutUser handler
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertActionStyle.default,
                                      handler: logoutUser))
        
        // if they cancel logout, do nothing
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: UIAlertActionStyle.cancel,
                                      handler: nil))
        
        // present alert to user
        self.present(alert, animated: true, completion: nil)
    }
    
    /**
     Logs out user and returns them to the WelcomeViewController. Uses Firebase auth().signout() method.
     
     - TODO: Review this method; don't need to clear all UserDefaults
     */
    func logoutUser (_action : UIAlertAction) {

        // Log out the user and send them back to WelcomeViewController
        do {
            try Auth.auth().signOut()
            self.performSegue(withIdentifier: "logoutSegue", sender: self)
            
            /* CLEAR USER PREFERENCES */
            
            let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            dictionary.keys.forEach { key in
                defaults.removeObject(forKey: key)
            }
            
            print("\nUser logged out.")
        }
        catch {
            print("\nError logging out.")
        }
        
    }
}
