//
//  ChangePatientsViewController.swift
//  PTAMA
//
//  Created by Andrew Whiskin on 20/5/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase

/**
 - TODO: Replace hardcoded values
 */
class ChangePatientsViewController: UITableViewController {

    var patients = [Patient]()
    var reference = DatabaseReference()
    
    func getPatients() {
        patients = [Patient]()
        let uid = Auth.auth().currentUser?.uid
        let caregiverReference = reference.child("caregivers").child(uid!).child("patients")
        caregiverReference.observeSingleEvent(of: .value) { (snapshot) in
            for child in snapshot.children {
                let snap = child as! DataSnapshot
                let patient = Patient(snapshot: snap)
                self.patients.append(patient)
            }
            self.patients.sort(by: { $0.name < $1.name })
            self.tableView.reloadData()
        }
    }
    
    /**
     Bit redundant - keep for later maybe
     */
    class Patient {
        /**
         - key: Firebase ID of task, e.g. patient1
         - name: Patient's name
         - age: Patient's age
         - rewardPoints: Total points patient currently has
         */
        
        var key = ""
        var name = ""
        var age = 0
        var rewardPoints = 0
        
        init(snapshot: DataSnapshot) {
            let dict = snapshot.value as! [String: Any]

            let snapKey = snapshot.key

            let name = dict["name"] as! String
            let age = dict["age"] as! Int
            let rewardPoints = dict["rewardPoints"] as! Int

            self.key = snapKey
            self.name = name
            self.age = age
            self.rewardPoints = rewardPoints
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewDidAppear(_ animated: Bool) {
        reference = Database.database().reference()
        DispatchQueue.main.async {
            self.getPatients()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return patients.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "labelCell")!
        
        if let patient = UserDefaults.standard.string(forKey: "currentPatient") {
            if patient == patients[indexPath.row].key {
                cell.accessoryType = .checkmark
            }
        }
        cell.textLabel?.text = patients[indexPath.row].name

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let patient = patients[indexPath.row]
        UserDefaults.standard.set(patient.key, forKey: "currentPatient")
        self.performSegue(withIdentifier: "patientsToHome", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let cell = tableView.cellForRow(at: indexPath)
        if cell?.accessoryType == .checkmark {
            return false
        }
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete task
            
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Delete patient", message: "Are you sure you want to delete the following profile?\n\n\(self.patients[indexPath.row].name)\n\nWARNING: This will remove also remove all tasks and rewards associated with the profile.", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "OK",
                                              style: UIAlertActionStyle.default,
                                              handler: { action in
                                                let key = self.patients[indexPath.row].key
                                                self.reference.child("caregivers").child((Auth.auth().currentUser?.uid)!).child("patients").child(key).removeValue()
                                                self.reference.child("tasks").child(key).removeValue()
                                                self.reference.child("rewards").child(key).removeValue()
                                                self.patients.remove(at: indexPath.row)
                                                self.tableView.deleteRows(at: [indexPath], with: .fade)
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel",
                                              style: UIAlertActionStyle.cancel,
                                              handler: nil))
                
                // present alert to user
                self.present(alert, animated: true, completion: nil)
            }
            
            
        }
    }
    
    func deletePatient(key: String) {
        
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
