//
//  OnboardingViewController.swift
//  PTAMA
//
//  Created by Joshua Truscott on 8/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import SwiftyOnboard

class OnboardingViewController: UIViewController {
    
    var swiftyOnboard: SwiftyOnboard!
    var titleArray: [String] = ["Complete tasks",
                                "Get rewards",
                                "Purchased rewards",
                                "Add tasks & rewards",
                                "Adding a new task",
                                "Deleting a task",
                                "Adding a new reward",
                                "Deleting a reward",
                                "Help"]
    var subTitleArray: [String] = ["Tap on a task to complete it,\n and earn reward points for each task\n you complete!",
                                   "In the rewards page, you can\n purchase rewards if you have enough points.",
                                   "A green badge appears showing how many\n you have purchased.",
                                   "Ask your caregiver to\n add new tasks and rewards for you\n via the profile page.",
                                   "Add a task via the profile page -\n tap the '+' icon and \nthis 'Add Task' screen appears.",
                                   "Swipe left on a task to delete it.",
                                   "Add a reward via the profile page -\n tap the '+' icon and \nthis 'Add Reward' screen appears",
                                   "Swipe left on a reward to delete it.",
                                   "To see this tutorial again,\n tap the '?' icon"]
    var imagesArray: [String] = ["screenshot-tasks",
                                 "screenshot-rewards",
                                 "screenshot-reward-purchased",
                                 "screenshot-profile",
                                 "screenshot-task-add",
                                 "screenshot-task-delete",
                                 "screenshot-reward-add",
                                 "screenshot-reward-delete",
                                 "screenshot-help"]
    
    var colors : [UIColor] = [#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
    
    var gradient: CAGradientLayer = {
        //gradient for the background view
        let blue = UIColor(red: 69/255, green: 127/255, blue: 202/255, alpha: 1.0).cgColor
        let purple = UIColor(red: 166/255, green: 172/255, blue: 236/255, alpha: 1.0).cgColor
        let gradient = CAGradientLayer()
        gradient.colors = [purple, blue]
        gradient.startPoint = CGPoint(x: 0.5, y: 0.18)
        return gradient
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGradient()
        UIApplication.shared.statusBarStyle = .lightContent
        
        swiftyOnboard = SwiftyOnboard(frame: view.frame, style: .dark)
        view.addSubview(swiftyOnboard)
        swiftyOnboard.dataSource = self
        swiftyOnboard.delegate = self
        UIApplication.shared.statusBarStyle = .default
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default // .default
    }
    
    func addGradient() {
        //Add the gradient to the view:
        self.gradient.frame = view.bounds
        view.layer.addSublayer(gradient)
    }
    
    @objc func handleSkip() {
        swiftyOnboard?.goToPage(index: 2, animated: true)
    }
    
    @objc func handleContinue(sender: UIButton) {
        let index = sender.tag
        swiftyOnboard?.goToPage(index: index + 1, animated: true)
    }
    
    @objc func handleGetStarted(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

extension OnboardingViewController: SwiftyOnboardDelegate, SwiftyOnboardDataSource {
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        //Number of pages in the onboarding:
        return titleArray.count
    }
    
    func swiftyOnboardBackgroundColorFor(_ swiftyOnboard: SwiftyOnboard, atIndex index: Int) -> UIColor? {
        //Return the background color for the page at index:
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let view = SwiftyOnboardPage()
        
        //Set the image on the page:
        view.imageView.image = UIImage(named: imagesArray[index])
        
        //Set the font and color for the labels:
        view.title.font = UIFont(name: "Lato-Heavy", size: 22)
        view.subTitle.font = UIFont(name: "Lato-Regular", size: 14)
        
        //Set the text in the page:
        view.title.text = titleArray[index]
        view.subTitle.text = subTitleArray[index]
        
        //Return the page for the given index:
        return view
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        let overlay = SwiftyOnboardOverlay()
        
        //Setup targets for the buttons on the overlay view:
        overlay.skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        
        overlay.continueButton.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
        
        //Setup for the overlay buttons:
        overlay.continueButton.titleLabel?.font = UIFont(name: "Lato-Bold", size: 16)
        overlay.continueButton.setTitleColor(#colorLiteral(red: 1, green: 0.9215686275, blue: 0.3490196078, alpha: 1), for: .normal)
        overlay.skipButton.setTitleColor(#colorLiteral(red: 1, green: 0.9215686275, blue: 0.3490196078, alpha: 1), for: .normal)
        overlay.skipButton.titleLabel?.font = UIFont(name: "Lato-Heavy", size: 16)
        
        //Return the overlay view:
        return overlay
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        let currentPage = round(position)
        overlay.pageControl.currentPage = Int(currentPage)
        overlay.continueButton.tag = Int(position)
        
        if currentPage == 0.0 || currentPage == 1.0 {
            overlay.continueButton.setTitle("Continue", for: .normal)
            
            overlay.skipButton.setTitle("SkipYA", for: .normal)
            overlay.skipButton.isHidden = true
        } else {
            overlay.continueButton.setTitle("Get Started!", for: .normal)
            overlay.continueButton.addTarget(self, action: #selector(handleGetStarted), for: .touchUpInside)
            overlay.skipButton.isHidden = true
        }
    }
}
