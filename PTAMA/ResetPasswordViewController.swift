//
//  ResetPasswordViewController.swift
//  PTAMA
//
//  Created by Gayathri Rajan on 16/5/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase

class ResetPasswordViewController: UIViewController {
    
    @IBOutlet var emailTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func resetButtonPressed(_ sender: AnyObject) {
        //To do
        resetPassword(email: emailTextfield.text!)
    }
    
    func resetPassword(email: String) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if error == nil {
                print("Email for password reset has been sent")
                // alert that reset password email is sent
                let alert = UIAlertController(title: "Email sent!", message: "Please check your email for information on how to reset your password.", preferredStyle: UIAlertControllerStyle.alert)
                
                // if they press OK, do nothing
                alert.addAction(UIAlertAction(title: "OK",
                                              style: UIAlertActionStyle.default, handler: nil))
                
                // present alert to user
                self.present(alert, animated: true, completion: nil)
                
            } else {
                print(error!)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
