//
//  WelcomeViewController.swift
//  PTAMA
//
//  Created by Gayathri Rajan on 24/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase

class WelcomeViewController: UIViewController {
    
    var ref : DatabaseReference?
    var handler : DatabaseHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ref = Database.database().reference()
        
        authenticateUser()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Check whether a Firebase user is currently signed in. If they are, segue to Tasks page.
     
     - TODO: create a function to retrieve the current user and current patient
     - TODO: create a function to assign a default patient if there is no current patient
     */
    func authenticateUser() {
        
        // check if there is a user signed in to Firebase
        if Auth.auth().currentUser != nil {
            
            // print UID, email address of current user
            // displayCurrentUserInfo()
            
            // segue to Home page
            if let patient = UserDefaults.standard.string(forKey: "currentPatient"), let cg = UserDefaults.standard.string(forKey: "currentCaregiver") {
                // All G
            } else {
                UserDefaults.standard.set("patient1", forKey: "currentPatient")
                UserDefaults.standard.set("1EC8K3XH7LaewaoXzZ1IGpjWSQu1", forKey: "currentCaregiver")
            }
            self.performSegue(withIdentifier: "goToHome", sender: self)
            
        } else {
            // No user is signed in.
            UserDefaults.standard.set("patient1", forKey: "currentPatient")
            UserDefaults.standard.set("1EC8K3XH7LaewaoXzZ1IGpjWSQu1", forKey: "currentCaregiver")
            // ...
        }
    }
    
    func displayCurrentUserInfo() {
        let user = Auth.auth().currentUser
        if let user = user {
            // The user's ID, unique to the Firebase project.
            // Do NOT use this value to authenticate with your backend server,
            // if you have one. Use getTokenWithCompletion:completion: instead.
            let uid = user.uid
            let email = user.email
            // ...
            print("\nCurrent user:",uid, email!)
        }
    }
}
