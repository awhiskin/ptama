//
//  RewardsViewController.swift
//  PTAMA
//
//  Created by Joshua Truscott on 8/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import FirebaseDatabase
import AVFoundation

class RewardsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    // Data model: These strings will be the data for the table view cells
    
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "rewardCell"
    
    var currentPoints = 0
    
    // don't forget to hook this up from the storyboard
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var currentPointLabel: UILabel!
    
    var rewards : [RewardClass] = [RewardClass]()
    var rewardsRef = DatabaseReference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        currentPointLabel.text = String(currentPoints)
        
        rewardsRef = Database.database().reference()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getRewards()
        getCurrentPoints()
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rewards.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! RewardsTableViewCell
        
        cell.selectionStyle = .none
        DispatchQueue.main.async {
            cell.setReward(title: self.rewards[indexPath.row].description,
                           currentPoints: self.currentPoints,
                           requiredPoints: self.rewards[indexPath.row].cost,
                           numAvailable: self.rewards[indexPath.row].numAvailable,
                           numPurchased: self.rewards[indexPath.row].numPurchased)
        }
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let reward : RewardClass = rewards[indexPath.row]
        if currentPoints >= reward.cost {
            displayPurchaseAlert(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView:UITableView, heightForRowAt indexPath: IndexPath)->CGFloat
    {
        return 120
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Used to store data retrieved from Firebase
    class RewardClass {
        /**
         - key: Firebase ID of task, e.g. Reward1.
         - cost: How many points the reward costs.
         - description: Description of the reward, e.g. "Get a new PS4".
                        Should enforce a 50-60 character limit to ensure the label doesn't cut off.
         - numAvailable: How many times the reward can be purchased.
                         -1 means a reward can be obtained unlimited number of times
         */
        
        var key = ""
        var cost = 0
        var description = ""
        var numAvailable = -1
        var numPurchased = 0
        
        init(snapshot: DataSnapshot) {
            let dict = snapshot.value as! [String: Any]
            
            self.key = snapshot.key
            
            if let cost = dict["cost"] {
                self.cost = cost as! Int
            }
            if let description = dict["description"] {
                self.description = description as! String
            }
            if let numAvailable = dict["numAvailable"] {
                self.numAvailable = numAvailable as! Int
            }
            if let numPurchased = dict["numPurchased"] {
                self.numPurchased = numPurchased as! Int
            }
        }
    }
    
    /**
     Handle purchasing rewards. Deduct points from currentPoints and remove reward if numAvailable < 1.
     
     - TODO: Implement a way to display the reward has been purchased (i.e. currently rewards simply disappear if you purchase them)
     */
    func purchaseReward(reward: RewardClass, index: Int) {
        if reward.numAvailable > 0 {
            reward.numAvailable -= 1
        }
        
        reward.numPurchased += 1
        
        DispatchQueue.main.async {
            self.subtractFromCurrentPoints(value: reward.cost)
            self.updateRewardInFirebase(reward: reward, currentPoints: self.currentPoints)
            AudioServicesPlayAlertSound(SystemSoundID(1109))
            self.tableView.reloadData()
            self.currentPointLabel.text = "\(self.currentPoints)"
        }
    }
    
    func subtractFromCurrentPoints(value: Int) {
        self.currentPoints -= value
    }
    
    /**
     Write changes to Firebase - update patient's current reward points + update remaining number available for this reward.
     */
    func updateRewardInFirebase(reward: RewardClass, currentPoints: Int) {
        let currentPatient = UserDefaults.standard.string(forKey: "currentPatient")!
        let currentCaregiver = UserDefaults.standard.string(forKey: "currentCaregiver")!
        rewardsRef.child("caregivers/\(currentCaregiver)/patients/\(currentPatient)/rewardPoints").setValue(self.currentPoints)
        rewardsRef.child("rewards/\(currentPatient)/\(reward.key)/numAvailable").setValue(reward.numAvailable)
        rewardsRef.child("rewards/\(currentPatient)/\(reward.key)/numPurchased").setValue(reward.numPurchased)
    }
    
    /**
     Get all rewards currentPatient has access to.
     */
    func getRewards() {
        rewards = [RewardClass]()
        let currentPatient = UserDefaults.standard.string(forKey: "currentPatient")
        let rewardsRef = self.rewardsRef.child("rewards").child(currentPatient!)
        rewardsRef.observeSingleEvent(of: .value, with: { snapshot in
            
            for child in snapshot.children {
                let snap = child as! DataSnapshot
                let reward = RewardClass(snapshot: snap)
                self.rewards.append(reward)
//                if (reward.numAvailable == -1 || reward.numAvailable > 0) {
//                    self.rewards.append(reward)
//                }
            }
            self.rewards.sort(by: { $0.cost < $1.cost })
            self.tableView.reloadData()
        })
    }
    
    /**
     Get number of rewardPoints that the currentPatient has accrued.
     */
    func getCurrentPoints() {
        let currentPatient = UserDefaults.standard.string(forKey: "currentPatient")
        let currentCaregiver = UserDefaults.standard.string(forKey: "currentCaregiver")
        
        let pointsRef = self.rewardsRef.child("caregivers").child(currentCaregiver!).child("patients").child(currentPatient!)
        pointsRef.observeSingleEvent(of: .value, with: { snapshot in
            if let data = snapshot.value as? [String : AnyObject] {
                let points = data["rewardPoints"] as! Int
                self.currentPoints = points
                self.currentPointLabel.text = String(points)
            }
            self.tableView.reloadData()
        })
    }
    
    /**
     Display prompt to confirm reward purchase.
     */
    func displayPurchaseAlert(indexPath: IndexPath) {
        let reward : RewardClass = rewards[indexPath.row]
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Purchase reward", message: "Are you sure you want to purchase the following reward?\n\n\(reward.description)", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK",
                                          style: UIAlertActionStyle.default,
                                          handler: { action in
                                            self.purchaseReward(reward: reward, index: indexPath.row)
            }))
        
            alert.addAction(UIAlertAction(title: "Cancel",
                                          style: UIAlertActionStyle.cancel,
                                          handler: nil))
            
            // present alert to user
            self.present(alert, animated: true, completion: nil)
        }
    }
}



