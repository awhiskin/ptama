//
//  LogInViewController.swift
//  PTAMA
//
//  Created by Gayathri Rajan on 24/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase

class LogInViewController: BaseViewController {

    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var passwordTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextfield.delegate = self
        passwordTextfield.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.restorationIdentifier == "email" {
            textField.resignFirstResponder()
            passwordTextfield.becomeFirstResponder()
            return false
        }
        return true
    }


    @IBAction func logInButtonPressed(_ sender: AnyObject) {
        loginUser()
        
    }
    
    @IBAction func primaryTrigger(_ sender: UITextField) {
        loginUser()
    }
    
    func loginUser() {
        print("\nAttempting to log in user.")
        
        // Log in the user
        Auth.auth().signIn(withEmail: emailTextfield.text!, password: passwordTextfield.text!) { (user, error) in
            
            if error != nil {
                print("\nFailed to log in user.\n")
                print(error!)
                
                // alert if error with login
                let alert = UIAlertController(title: "Sorry couldn't log you in!", message: "You have entered the wrong email or password. Please try again. If you forgot your password tap on 'Reset Password?' under 'Log In'.", preferredStyle: UIAlertControllerStyle.alert)
                
                // if they press OK, do nothing
                alert.addAction(UIAlertAction(title: "OK",
                                              style: UIAlertActionStyle.default, handler: nil))
                
                // present alert to user
                self.present(alert, animated: true, completion: nil)
                
            } else {
                print("Successfully logged in user.")
                UserDefaults.standard.set(user?.user.uid, forKey: "currentCaregiver")
                self.performSegue(withIdentifier: "goToPatients", sender: self)
            }
            
        }
    }
}
