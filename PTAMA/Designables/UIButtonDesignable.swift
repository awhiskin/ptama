//
//  UIButtonDesignable.swift
//  PTAMA
//
//  Created by Andrew Whiskin on 10/5/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit

@IBDesignable class UIButtonDesignable: UIButton {

    @IBInspectable override var backgroundColor: UIColor? {
        didSet {
            layer.backgroundColor = backgroundColor?.cgColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }

}
