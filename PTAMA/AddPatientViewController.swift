//
//  AddPatientViewController.swift
//  PTAMA
//
//  Created by Gayathri Rajan on 27/5/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase

class AddPatientViewController: BaseViewController {
    
    let databaseRef = Database.database().reference(fromURL: "https://ptama-7f193.firebaseio.com/")

    @IBOutlet var patientNameTextfield: UITextField!
    @IBOutlet var patientAgeTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addPatientButtonPressed(_ sender: AnyObject) {
        addPatient()
        
    }
    
    func addPatient() {
        // Patient data
        let patientName = patientNameTextfield.text
        let patientAge = Int(patientAgeTextfield.text!)
        let rewardPoints = 0
        
        let patientValues = ["name": patientName!, "age": patientAge ?? -1, "rewardPoints": rewardPoints] as [String : Any]
        
        let userReference = self.databaseRef.child("caregivers").child(Auth.auth().currentUser!.uid)
        let patientReference = userReference.child("patients").childByAutoId()
        
        patientReference.updateChildValues(patientValues
            , withCompletionBlock: { (error, ref) in
                if error != nil {
                    print(error!)
                    return
                }
                UserDefaults.standard.set(patientReference.key, forKey: "currentPatient")
                self.performSegue(withIdentifier: "goToHome", sender: self)
        })
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
