//
//  TasksTableViewController.swift
//  PTAMA
//
//  Created by Joshua Truscott on 4/6/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase

class TasksTableViewController: UITableViewController {

    var reference = DatabaseReference()
    
    var tasks = [Task]()
    var defaults = UserDefaults.standard
    
    var currentPatient : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reference = Database.database().reference()
        currentPatient = UserDefaults.standard.string(forKey: "currentPatient")
        fetchTasks()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tasks = []
        fetchTasks()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tasks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "basicCell")!
        cell.textLabel?.text = tasks[indexPath.row].description

        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete task
            self.reference.child("tasks").child(currentPatient!).child(tasks[indexPath.row].key).removeValue()
            tasks.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func fetchTasks() {
        self.tasks = []
        let tasksRef = self.reference.child("tasks").child(currentPatient!)
        tasksRef.observeSingleEvent(of: .value) { (snapshot) in
            for child in snapshot.children {
                
                let snap = child as! DataSnapshot
                let task = Task(snapshot: snap)
                self.tasks.append(task)
            }
            // self.tasks.sort(by: { $0.time > $1.time })
            self.tableView.reloadData()
        }
    }
    
    /** Used to store data retrieved from Firebase
     - TODO: restructure Task object to fit new proposed JSON changes (i.e. store reminders within Task)
     */
    class Task {
        /**
         - key: Firebase ID of task, e.g. Task1.
         - description: Description of the task, e.g. "Take your meds".
         - startDate: Starting date of the task.
         - endDate: End date of the task. After this date the task should not appear in the Task list.
         - points: Number of points earned for completing task.
         - time: What time the task is due - e.g. "4:00PM" stored as "2018-05-15 16:00:00".
         */
        
        var key = ""
        var description = ""
        var points = 0
        var time = ""
        var days = ""
        
        init(snapshot: DataSnapshot) {
            let dict = snapshot.value as! [String: Any]
            
            let snapKey = snapshot.key
            
            let description = dict["description"] as! String
            let points = dict["points"] as! Int
            let time = dict["time"] as! String
            let days = dict["days"] as! String
            
            self.key = snapKey
            self.description = description
            self.points = points
            self.time = time
            self.days = days
        }
        
        /**
         Returns time string formatted as 12hr AM/PM time.
         
         - returns: Time formatted for use within TasksTableViewCell.
         */
        func getFormattedTime() -> String {
            if self.time != "" {
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm:ss"
                let time = formatter.date(from: self.time)
                formatter.dateFormat = "h:mm a"
                let formattedTime = formatter.string(from: time!)
                return formattedTime
            } else {
                return "00:00 AM"
            }
        }
    }
}
