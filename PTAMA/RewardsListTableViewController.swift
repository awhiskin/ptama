
//
//  Created by Joshua Truscott on 4/6/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase

class RewardsListTableViewController: UITableViewController {
    
    var reference = DatabaseReference()
    
    var rewards = [RewardClass]()
    var defaults = UserDefaults.standard
    
    var currentPatient : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reference = Database.database().reference()
        currentPatient = UserDefaults.standard.string(forKey: "currentPatient")
        fetchRewards()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.rewards = []
        fetchRewards()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return rewards.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "basicCell")!
        cell.textLabel?.text = rewards[indexPath.row].description
        
        // Configure the cell...
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.reference.child("rewards").child(currentPatient!).child(rewards[indexPath.row].key).removeValue()
            rewards.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func fetchRewards() {
        self.rewards = []
        let tasksRef = self.reference.child("rewards").child(currentPatient!)
        tasksRef.observeSingleEvent(of: .value) { (snapshot) in
            for child in snapshot.children {
                
                let snap = child as! DataSnapshot
                let reward = RewardClass(snapshot: snap)
                self.rewards.append(reward)
            }
            // self.tasks.sort(by: { $0.time > $1.time })
            self.tableView.reloadData()
        }
    }
    
    class RewardClass {
        /**
         - key: Firebase ID of task, e.g. Reward1.
         - cost: How many points the reward costs.
         - description: Description of the reward, e.g. "Get a new PS4".
         Should enforce a 50-60 character limit to ensure the label doesn't cut off.
         - numAvailable: How many times the reward can be purchased.
         -1 means a reward can be obtained unlimited number of times
         */
        
        var key = ""
        var cost = 0
        var description = ""
        var numAvailable = -1
        
        init(snapshot: DataSnapshot) {
            let dict = snapshot.value as! [String: Any]
            
            let snapKey = snapshot.key
            
            let rewardCost = dict["cost"] as! Int
            let rewardName = dict["description"] as! String
            let rewardNumAvailable = dict["numAvailable"] as! Int
            
            self.key = snapKey
            self.cost = rewardCost
            self.description = rewardName
            self.numAvailable = rewardNumAvailable
        }
    }
}

