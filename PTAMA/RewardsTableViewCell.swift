//
//  RewardsTableViewCell.swift
//  PTAMA
//
//  Created by Joshua Truscott on 9/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import LinearProgressBar

class RewardsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var rewardView: UIView!
    @IBOutlet weak var rewardLabel: UILabel!
    @IBOutlet weak var progressBar: RewardProgressBar!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var starLabel: UILabel!
    @IBOutlet weak var rewardStarImage: UIImageView!
    @IBOutlet weak var numAvailLabel: UILabel!
    @IBOutlet weak var numPurchasedLabel: UILabel!
    @IBOutlet weak var numPurchasedBadge: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        rewardView.layer.cornerRadius = 10
//        rewardView.layer.shadowColor = UIColor.black.cgColor
//        rewardView.layer.shadowOffset = CGSize(width: 3, height: 3)
//        rewardView.layer.shadowOpacity = 0.25
//        rewardView.layer.shadowRadius = 4.0
    }
    
    func setReward(title: String, currentPoints: Int, requiredPoints: Int, numAvailable: Int, numPurchased: Int) {
        DispatchQueue.main.async {
            self.rewardLabel.text = title
            var points = currentPoints
            if(points > requiredPoints) {
                points = requiredPoints
            }
            self.percentageLabel.text = "\(points)/\(requiredPoints)"
            self.progressBar.progressValue = CGFloat((Double(points)/Double(requiredPoints))*100.0)
            self.starLabel.text = String(requiredPoints)
            if numAvailable == -1 {
                self.numAvailLabel.text = "∞"
            } else {
                self.numAvailLabel.text = "\(numAvailable)"
            }
            
            if (numPurchased > 0) {
                self.numPurchasedBadge.isHidden = false
                self.numPurchasedLabel.isHidden = false
                self.numPurchasedLabel.text = "\(numPurchased)"
            } else {
                self.numPurchasedBadge.isHidden = true
                self.numPurchasedLabel.isHidden = true
            }
            
            if (self.progressBar.progressValue != 100) || (numAvailable == 0) {
                self.progressBar.barColor = #colorLiteral(red: 0.7966429188, green: 0.7966429188, blue: 0.7966429188, alpha: 1)
                self.rewardStarImage.image = #imageLiteral(resourceName: "graphic-star-desaturated")
            } else {
                self.progressBar.barColor = #colorLiteral(red: 1, green: 0.9215686275, blue: 0.3490196078, alpha: 1)
                self.rewardStarImage.image = #imageLiteral(resourceName: "graphic-star")
            }
        }
    }

}

class RewardProgressBar: LinearProgressBar {
    
}
