//
//  TasksViewController.swift
//  PTAMA
//
//  Created by Joshua Truscott on 8/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation
import UserNotifications

class TasksViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    var reference = DatabaseReference()
    var tasks = [Task]()
    var defaults = UserDefaults.standard
    let center = UNUserNotificationCenter.current()
    var currentPatient : String?
    
    @IBOutlet var tableView: UITableView!
    
    // TASK
    
    // Data model: These strings will be the data for the table view cells
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "taskCell"
    
    // don't forget to hook this up from the storyboard
    
    func enableNotifications() {
        center.requestAuthorization(options: [.alert, .sound]) {
            (granted, error) in
        }
    }
    
    
    
    func setNotifications() {
        center.getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                let calendar = Calendar.current
                for task in self.tasks {
                    for day in task.days {
                        let content = UNMutableNotificationContent()
                        content.title = "Task Due!"
                        content.body = task.description
                        content.sound = UNNotificationSound.default()
                        
                        let formatter = DateFormatter()
                        formatter.dateFormat = "HH:mm:ss"
                        let time = formatter.date(from: task.time)
                        
                        var date = calendar.date(byAdding: DateComponents(hour: calendar.component(.hour, from: time!), minute: calendar.component(.minute, from: time!), second: calendar.component(.second, from: time!)), to: calendar.startOfDay(for: Date()))
                        
                        date = calendar.date(bySetting: .weekday, value: Int(String(day))!, of: date!)
                        
                        
                        let triggerWeekly = Calendar.current.dateComponents([.weekday,.hour,.minute,.second,], from: date!)
                        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
                        
                        let identifier = "ptama-task-" + task.key
                        let request = UNNotificationRequest(identifier: identifier,
                                                            content: content, trigger: trigger)
                        
                        self.center.add(request, withCompletionHandler: { (error) in
                            if let error = error {
                                print(error)
                            } else {
                            }
                        })
                        
                    }
                }
                // Notifications allowed
                
                // Notification content
                
                
                
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        reference = Database.database().reference()
        enableNotifications()
        setNotifications()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getCurrentPoints()
        fetchTasks()
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "presentTaskPopover", sender: indexPath.row)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presentTaskPopover" {
            let indexPath = sender as! Int
            let task : Task = tasks[indexPath]
            let receiverVC = segue.destination as! TaskPopoverViewController
            receiverVC.taskDescription = task.description
            receiverVC.taskPoints = task.points
            receiverVC.taskTime = task.time
            receiverVC.taskKey = task.key
            receiverVC.indexPath = indexPath
        }
    }
    
    func completeTask(indexPath: Int) {
        let task = tasks[indexPath]
        DispatchQueue.main.async {
            self.tasks.remove(at: indexPath)
            self.tableView.reloadData()
            self.addPoints(points: task.points)
            self.updateTaskInFirebase(task: task)
            AudioServicesPlayAlertSound(SystemSoundID(1109))
        }
    }
    
    func updateTaskInFirebase(task: Task) {
        let currentPatient = UserDefaults.standard.string(forKey: "currentPatient")
        
        self.reference.child("tasks").child(currentPatient!).child("\(task.key)/completed").setValue(true)
    }
    
    /**
     Get number of rewardPoints that the currentPatient has accrued.
     */
    
    var currentPoints = 0
    
    func getCurrentPoints() {
        let currentPatient = UserDefaults.standard.string(forKey: "currentPatient")
        let currentCaregiver = UserDefaults.standard.string(forKey: "currentCaregiver")
        
        let pointsRef = self.reference.child("caregivers").child(currentCaregiver!).child("patients").child(currentPatient!)
        pointsRef.observeSingleEvent(of: .value, with: { snapshot in
            if let data = snapshot.value as? [String : AnyObject] {
                let points = data["rewardPoints"] as! Int
                self.currentPoints = points
                print("Current Points: ", self.currentPoints)
            }
            self.tableView.reloadData()
        })
    }
    
    /**
     Write changes to Firebase - update patient's current reward points
     */
    func addPoints(points: Int) {
        self.currentPoints += points
        
        let currentPatient = UserDefaults.standard.string(forKey: "currentPatient")!
        let currentCaregiver = UserDefaults.standard.string(forKey: "currentCaregiver")!
        
        reference.child("caregivers/\(currentCaregiver)/patients/\(currentPatient)/rewardPoints").setValue(currentPoints)
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TasksTableViewCell
        cell.selectionStyle = .none
        DispatchQueue.main.async {
            cell.setTask(taskTitle: self.tasks[indexPath.row].description,
                         rewardPoints: self.tasks[indexPath.row].points,
                         timeDue: self.tasks[indexPath.row].getFormattedTime())
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView:UITableView, heightForRowAt indexPath: IndexPath)->CGFloat
    {
        return 120
    }
    
    /**
     Get all tasks currentPatient has access to.
     
     - TODO: change currentPatient to read from UserDefaults instead of hard-coding
     - TODO: fix sorting - needs to sort by time of day (e.g. 07:00, 12:00, 15:00)
     */
    func fetchTasks() {
        let day = String(Calendar.current.component(.weekday, from: Date()))
        let currentPatient = UserDefaults.standard.string(forKey: "currentPatient")
        let tasksRef = self.reference.child("tasks").child(currentPatient!)
        tasksRef.observeSingleEvent(of: .value) { (snapshot) in
            self.tasks = []
            for child in snapshot.children {
                
                let snap = child as! DataSnapshot
                let task = Task(snapshot: snap)
//                self.tasks.append(task)
                if (task.days.contains(day)) && (task.completed == false) {
                    self.tasks.append(task)
                }
            }
            self.setNotifications()
            self.tasks.sort(by: { $0.time < $1.time })
            self.tableView.reloadData()
        }
    }
    
    /** Used to store data retrieved from Firebase
     - TODO: restructure Task object to fit new proposed JSON changes (i.e. store reminders within Task)
     */
    class Task {
        /**
         - key: Firebase ID of task, e.g. Task1.
         - description: Description of the task, e.g. "Take your meds".
         - startDate: Starting date of the task.
         - endDate: End date of the task. After this date the task should not appear in the Task list.
         - points: Number of points earned for completing task.
         - time: What time the task is due - e.g. "4:00PM" stored as "2018-05-15 16:00:00".
         */
        
        var key = ""
        var description = ""
        var points = 0
        var time = ""
        var days = ""
        var completed = false

        init(snapshot: DataSnapshot) {
            let dict = snapshot.value as! [String: Any]
            
            self.key = snapshot.key
            
            if let description = dict["description"] {
                self.description = description as! String
            }
            if let points = dict["points"] {
                self.points = points as! Int
            }
            if let time = dict["time"] {
                self.time = time as! String
            }
            if let days = dict["days"] {
                self.days = days as! String
            }
            if let completed = dict["completed"] {
                self.completed = completed as! Bool
            }
        }
        
        /**
         Returns time string formatted as 12hr AM/PM time.
         
         - returns: Time formatted for use within TasksTableViewCell.
         */
        func getFormattedTime() -> String {
            if self.time != "" {
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm:ss"
                let time = formatter.date(from: self.time)
                formatter.dateFormat = "h:mm a"
                let formattedTime = formatter.string(from: time!)
                return formattedTime
            } else {
                return "00:00 AM"
            }
        }
    }
}
