//
//  Extensions.swift
//  PTAMA
//
//  Created by Joshua Truscott on 9/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit

// Modified navigation bar to match theme
class BaseViewController: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if let navigationController = self.navigationController {
//            let tintColor = UIColor(red: 254/255.0, green: 73/255.0, blue: 73/255.0, alpha: 1.0)
//            let navigationBar = navigationController.navigationBar
//            let navBorder: UIView = UIView(frame: CGRect(x:0, y:navigationBar.frame.size.height - 1, width:navigationBar.frame.size.width, height:5))
//            navBorder.backgroundColor = tintColor
//            navBorder.isOpaque = true
//            self.navigationController?.navigationBar.addSubview(navBorder)
//            self.navigationController?.navigationBar.tintColor = tintColor
//            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: tintColor]
//
//            self.navigationController?.navigationBar.prefersLargeTitles = true
//            self.navigationItem.largeTitleDisplayMode = .always
//        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = CGRect(x:self.view.frame.origin.x, y:self.view.frame.origin.y - 50, width:self.view.frame.size.width, height:self.view.frame.size.height + 50);
            
        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = CGRect(x:self.view.frame.origin.x, y:self.view.frame.origin.y + 50, width:self.view.frame.size.width, height:self.view.frame.size.height - 50);
            
        })
    }
}
