//
//  AddTaskViewController.swift
//  PTAMA
//
//  Created by Joshua Truscott on 22/5/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

class AddTasksViewController: UITableViewController {
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var points: UISegmentedControl!
    @IBOutlet weak var time: UIDatePicker!
    @IBOutlet weak var days: MultiSelectSegmentedControl!
    let center = UNUserNotificationCenter.current()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let saveTaskButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveTask))
        self.navigationItem.rightBarButtonItem = saveTaskButton
        self.name.addDoneButtonOnKeyboard()
        // Do any additional setup after loading the view.
    }
    
    @objc func saveTask() {
        if let patient = UserDefaults.standard.string(forKey: "currentPatient") {
            let descriptionString = self.name.text ?? ""
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm:ss"
            let timeString = formatter.string(from: self.time.date)
            print("task time")
            print(timeString)
            var daysString = ""
            // get days
            days.selectedSegmentIndexes.forEach { (index) in
                daysString.append(String(index+1))
            }
            let taskPoints : Int = Int(points.titleForSegment(at: points.selectedSegmentIndex)!)!
            let taskValues = ["description": name.text as Any,
                              "points": taskPoints,
                              "days": daysString,
                              "time": timeString] as [String : Any]
            let ref = Database.database().reference().child("tasks").child(patient).childByAutoId()
            ref.updateChildValues(taskValues
                , withCompletionBlock: { (error, ref) in
                    if error != nil {
                        print(error!)
                        return
                    }
                    UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                        if settings.authorizationStatus == .authorized {
                            let calendar = Calendar.current
                            for day in daysString {
                                let content = UNMutableNotificationContent()
                                content.title = "Task Due!"
                                content.body = descriptionString
                                content.sound = UNNotificationSound.default()
                                
                                let formatter = DateFormatter()
                                formatter.dateFormat = "HH:mm:ss"
                                let time = formatter.date(from: timeString)
                                
                                var date = calendar.date(byAdding: DateComponents(hour: calendar.component(.hour, from: time!), minute: calendar.component(.minute, from: time!), second: calendar.component(.second, from: time!)), to: calendar.startOfDay(for: Date()))
                                
                                date = calendar.date(bySetting: .weekday, value: Int(String(day))!, of: date!)
                                
                                
                                let triggerWeekly = Calendar.current.dateComponents([.weekday,.hour,.minute,.second,], from: date!)
                                let trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
                                
                                let identifier = "ptama-task-" + ref.key
                                let request = UNNotificationRequest(identifier: identifier,
                                                                    content: content, trigger: trigger)
                                
                                self.center.add(request, withCompletionHandler: { (error) in
                                    print("Added new notification")
                                })
                                
                            }
                            
                        }
                        
                    }
                    
                    if let navController = self.navigationController {
                        navController.popViewController(animated: true)
                    }
                    })}
    }
}

extension UITextField{
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}
