//
//  ProfilePopupVC.swift
//  PTAMA
//
//  Created by Andrew Whiskin on 13/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit

class ProfilePopupVC: UIViewController {
    @IBAction func dismissPopup(_ sender: UIButton) {
        dismissProfilePopup()
    }
    
    @IBAction func popupYes(_ sender: UIButton) {
        dismissProfilePopup()
    }
    func dismissProfilePopup() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
