//
//  RegisterViewController.swift
//  PTAMA
//
//  Created by Gayathri Rajan on 24/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: BaseViewController {

    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var passwordTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextfield.delegate = self
        passwordTextfield.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.restorationIdentifier == "email" {
            textField.resignFirstResponder()
            passwordTextfield.becomeFirstResponder()
            return false
        }
        return true
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func registerButtonPressed(_ sender: AnyObject) {
        registerUser()
    }
    
    func registerUser() {
        // Set up a new user on our Firebase database
        Auth.auth().createUser(withEmail: emailTextfield.text!, password: passwordTextfield.text!) { (user, error) in
            
            if error != nil {
                print(error!)
            } else {
                //successful
                print("\nSuccessfully registered user!")
                //self.performSegue(withIdentifier: "goToAddPatient", sender: self)
               self.loginUser(email: self.emailTextfield.text!,password: self.passwordTextfield.text!)
            }
        }
    }
    
    func loginUser(email: String, password: String) {
        print("Attempting to log in user.")

        // Log in the user
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                print("Failed to log in user.")
                print(error!)
            } else {
                print("Successfully logged in user.")
                UserDefaults.standard.set(user?.user.uid, forKey: "currentCaregiver")
                self.performSegue(withIdentifier: "goToAddPatient", sender: self)
            }

        }
    }
}
