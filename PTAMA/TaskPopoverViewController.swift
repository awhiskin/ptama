//
//  TaskPopoverViewController.swift
//  PTAMA
//
//  Created by Andrew Whiskin on 5/6/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit

class TaskPopoverViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var taskDescriptionLabel: UILabel!
    
    var taskDescription : String?
    var taskPoints : Int?
    var taskTime : String?
    var taskKey : String?
    var indexPath : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.layer.cornerRadius = 10

        taskDescriptionLabel.text = self.taskDescription
        pointsLabel.text = "\(self.taskPoints!) points"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func outsideButton(_ sender: Any) {
        dismissPopover()
    }
    @IBAction func completeTaskButton(_ sender: Any) {
        dismissPopover()
        let navController = self.presentingViewController as! UINavigationController
        let parent = navController.childViewControllers[0] as! TasksViewController
        parent.completeTask(indexPath: indexPath!)
    }
    
    func dismissPopover() {
        self.dismiss(animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
