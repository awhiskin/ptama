//
//  RewardsTableViewCell.swift
//  PTAMA
//
//  Created by Joshua Truscott on 9/4/18.
//  Copyright © 2018 MQ Uni Pace Project. All rights reserved.
//

import UIKit
import LinearProgressBar

class TasksTableViewCell: UITableViewCell {
    
    @IBOutlet weak var taskView: UIView!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var rewardLabel: UILabel!
    @IBOutlet weak var timeDueLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        taskView.layer.cornerRadius = 10
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setTask(taskTitle: String, rewardPoints: Int, timeDue: String) {
        DispatchQueue.main.async {
            self.taskLabel.text = taskTitle
            self.timeDueLabel.text = timeDue
            self.rewardLabel.text = String(rewardPoints) + " Points"
        }
    }
    
}
