# PTAMA
Open PTAMA.xcworkspace in Xcode

Cocoapods
Allows us to add/remove dependencies to the project easily

Install (Mac OS X):
sudo gem install cocoapods

Install dependencies in Podfile:
pod install / pod update (updates all to newest versions)

Modules used:
SwiftyOnboard - https://github.com/juanpablofernandez/SwiftyOnboard
